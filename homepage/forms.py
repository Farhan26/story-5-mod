from django import forms
from .models import Schedule
from django.utils.translation import ugettext_lazy as _



class ScheduleForm(forms.ModelForm):
    class Meta:
        model = Schedule
        fields = ['activity', 'date', 'time', 'place', 'category']
        widgets = {
            'date': forms.DateInput( attrs={'type': 'date'}),
            'time': forms.TimeInput(attrs={'type': 'time'}),
            'activity': forms.TextInput(attrs={'placeholder':'Type your activity'})}
        labels = {
            'activity':_('Activity'),
            'date':_('Date'),
            'time':_('Time')
            }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })
