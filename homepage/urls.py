from django.urls import path
from django.conf.urls import url
from . import views
from .views import *


urlpatterns = [
     url(r"^deleteurl/(?P<delete_id>\d+)/$",views.delete, name='delete'),
    path('', views.home, name='home'),
    path('profile', views.profile, name='profile'),
    path('experiences', views.experiences, name='experiences'),
    path('schedule', views.schedule, name='schedule'),
    path('schedule/create', views.schedule_create, name='schedule_create'),
    path('schedule/delete', views.schedule_delete, name='schedule_delete'),

]