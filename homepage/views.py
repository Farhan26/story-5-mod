from django.shortcuts import render,redirect
from django.http import HttpResponse
from .models import Schedule
from . import forms


def home(request):
	return render(request,'fix1.html')

def profile(request):
	return render(request,'fix2.html')

def experiences(request):
	return render(request,'fix3.html') 

def schedule(request):
    schedules = Schedule.objects.all().order_by('date')
    return render(request, 'schedule.html', {'schedules': schedules})

def schedule_create(request):
    if request.method == 'POST':
        form = forms.ScheduleForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('schedule')

    else:
        form = forms.ScheduleForm()
    return render(request, 'create.html', {'form': form})

def schedule_delete(request):
	Schedule.objects.all().delete()
	return render(request, "schedule.html")

def delete(request,delete_id):
    Schedule.objects.filter(id=delete_id).delete()
    return redirect('schedule')
